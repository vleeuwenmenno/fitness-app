import 'package:flutter/material.dart';
import 'profile.dart';

class ProfilePage extends StatelessWidget 
{
    final List userData;
    ProfilePage(this.userData);

    @override
    Widget build(BuildContext context) 
    {
        Profile profile = parseUserData(userData);

        return new Scaffold(
            body: new ListView(
                padding: const EdgeInsets.all(0.0),
                children: <Widget>[
                    new ProfileHeader(profile),
                ],
            ),
        );
    }
}

class ProfileHeader extends StatelessWidget 
{
  final Profile profile;

  const ProfileHeader(this.profile);

  @override
  Widget build(BuildContext context) {
    final topPadding = MediaQuery
        .of(context)
        .padding
        .top;

    const headerHeight = 160.0;

    return new Container(
      height: headerHeight,
      child: new Stack(
        fit: StackFit.expand,
        children: <Widget>[
          // linear gradient
          new Container(
            height: headerHeight,
            decoration: new BoxDecoration(
              gradient: new LinearGradient(
                  colors: <Color>[ //7928D1
                    const Color(0xFF7928D1), const Color(0xFF9A4DFF)],
                  stops: <double>[0.3, 0.5],
                  begin: Alignment.topRight, end: Alignment.bottomLeft
              ),
            ),
          ),
          new Padding(padding: EdgeInsets.only(top: headerHeight),
            child: new Column(
              children: <Widget>[
                new Text ("No history available..."),
              ],
            ),
          ),

          // radial gradient

          new Padding(
            padding: new EdgeInsets.only(
                top: topPadding, left: 15.0, right: 15.0, bottom: 20.0),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
//                _buildBellIcon(),
                new Padding(
                  padding: const EdgeInsets.only(bottom: 15.0),
                ),
                new Padding(
                  padding: const EdgeInsets.only(bottom: 20.0),
                  child: _buildAvatar(),
                ),
                _buildTotalTimetats()
              ],
            ),
          ),
        ],
      ),
    );
  }


  Widget _buildAvatar() {
    final mainTextStyle = new TextStyle(
        color: Colors.white,
        fontWeight: FontWeight.w700,
        fontSize: 20.0);
    final subTextStyle = new TextStyle(
        fontSize: 16.0,
        color: Colors.white70,
        fontWeight: FontWeight.w700);

    return new Row(
      children: <Widget>[
        new Container(
          width: 70.0, height: 60.0,
          decoration: new BoxDecoration(
            image: new DecorationImage(
                image: new AssetImage('assets/avatar.png'),
                fit: BoxFit.cover),
            borderRadius: new BorderRadius.all(new Radius.circular(20.0)),
          ),
        ),
        new Padding(padding: const EdgeInsets.only(right: 20.0)),
        new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Text(profile.fullName, style: mainTextStyle),
            new Text(profile.location, style: subTextStyle),
          ],
        ),
      ],
    );
  }

  Widget _buildTotalTimetats() {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        _buildTotalTimetat("Total Time", profile.totalTimeString),
        _buildVerticalDivider(),
        _buildTotalTimetat("Reached Goals", profile.reachedGoalsString),
        _buildVerticalDivider(),
        _buildTotalTimetat("Total Steps", profile.totalStepsString),
      ],
    );
  }

  Widget _buildTotalTimetat(String title, String value) {
    final titleStyle = new TextStyle(
        fontSize: 16.0,

        color: Colors.white);
    final valueStyle = new TextStyle(

        fontSize: 18.0,
        fontWeight: FontWeight.w700,
        color: Colors.white);
    return new Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        new Text(title, style: titleStyle),
        new Text(value, style: valueStyle),
      ],
    );
  }

  Widget _buildVerticalDivider() {
    return new Container(
      height: 30.0,
      width: 1.0,
      color: Colors.white30,
      margin: const EdgeInsets.only(left: 10.0, right: 10.0),
    );
  }
}

class ProfileAbout extends StatefulWidget {
  ProfileAbout({Key key}) : super(key: key);

  @override
  _ProfileAboutState createState() => new _ProfileAboutState();
}

class _ProfileAboutState extends State<ProfileAbout> {


  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new Center(
        child: new Text("Hello"),
      ),
    );
  }
}

