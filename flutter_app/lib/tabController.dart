import 'package:flutter/material.dart';
import 'statistics.dart';
import 'exerciseListView.dart';
import 'profilePage.dart';

class MainTabController extends StatefulWidget
{
    final List userData;
    const MainTabController(this.userData);

    @override
    _MainTabControllerState createState() => new _MainTabControllerState(userData);
}

class _MainTabControllerState extends State<MainTabController> with SingleTickerProviderStateMixin 
{
    final List userData;
    _MainTabControllerState(this.userData);

  final List<Tab> myTabs = <Tab>[
    new Tab(text: 'STATISTICS'),
    new Tab(text: 'MAIN'),
    new Tab(text: 'PROFILE')
  ];

  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: myTabs.length);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _tabController.animateTo(1);
    return new Scaffold(
      appBar: new AppBar(
        bottom: new TabBar(
          isScrollable: true,
          controller: _tabController,
          tabs: myTabs,
        ),
      ),
      body: new TabBarView(
        controller: _tabController,
        children: [
          Statistics(),
          ExerciseList(userData: userData),
          ProfilePage(userData)
        ],
      ),
    );
  }
}