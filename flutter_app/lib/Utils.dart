import 'dart:async';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'dart:io';

class Utils
{
    /// Assumes the given path is a text-file-asset.
    static Future<String> _getAssetData(String path) async 
    {
        return await rootBundle.loadString(path);
    }

    static Future<String> get _localPath async 
    {
        final directory = await getApplicationDocumentsDirectory();
        return directory.path;
    }

    static Future<File> _localFile(String filePath) async 
    {
        final path = await _localPath;
        return File('$path/$filePath');
    }

    static Future<File> writeStrFile(String str, String filePath, [bool overwrite = false]) async 
    {
        final file = await _localFile(filePath);

        if (await file.exists())
        {
            if (overwrite)
            {
                // Write the file
                return file.writeAsString('$str');
            }
            else
            {
                throw new Exception("File already exists");
            }
        }
        else
        {
            // Write the file
            return file.writeAsString('$str');
        }
    }

    static Future<int> readStrFile(String filePath) async 
    {
        try 
        {
            final file = await _localFile(filePath);

            // Read the file
            String contents = await file.readAsString();
            return int.parse(contents);
        } 
        catch (e) 
        {
            // If we encounter an error, return 0
            return 0;
        }
    }

    static Future<bool> strFileExists(String filePath) async 
    {
        try 
        {
            final file = await _localFile(filePath);

            // Read the file
            bool answer = await file.exists();
            return answer;
        } 
        catch (e) 
        {
            // If we encounter an error, return 0
            return false;
        }
    }
}