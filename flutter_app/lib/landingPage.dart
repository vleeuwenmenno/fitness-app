import 'package:flutter/material.dart';

import 'utils.dart';
import 'loginPage.dart';

class LandingPage extends StatefulWidget 
{
    const LandingPage({Key key}) : super(key: key);

    @override
    _LandingPageState createState() => new _LandingPageState();
}

class _LandingPageState extends State<LandingPage> with SingleTickerProviderStateMixin 
{
    TabController _tabController;

    bool showBackBtn = false;
    bool showFinishBtn = false;

    void _changed(bool visibility, String field) {
        setState(() {
            if (field == "backBtn")
                showBackBtn = visibility;
            else if (field == "finishBtn")
                showFinishBtn = visibility;
        });
    }

    final List<Tab> myTabs = <Tab>[
            new Tab(
                child: new Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                            new Icon(Icons.schedule, size: 196.0),
                            new Text("Follow pre-made workout schedules")
                        ],
                )
            ),
            new Tab(
                child: new Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                            new Icon(Icons.share, size: 196.0),
                            new Text("Share workout achievements with friends")
                        ],
                )
            ),
            new Tab(
                child: new Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                            new Icon(Icons.fitness_center, size: 196.0),
                            new Text("Monitor your pace, milage and more...")
                        ],
                )
            ),
        ];


    @override
    void initState() 
    {
        super.initState();

        _tabController = new TabController(vsync: this, length: myTabs.length);
    }

    @override
    void dispose() 
    {
        _tabController.dispose();
        super.dispose();
    }

    @override
    Widget build(BuildContext context) 
    {
        return new Scaffold(
            // appBar: new AppBar(
            //     title: new Text("Introduction"),
            // ),
            body: new Stack(
                children: <Widget>[
                    new TabBarView(
                        physics: new NeverScrollableScrollPhysics(),
                        controller: _tabController,
                        children:
                            myTabs.map((Tab tab) {
                                return tab;
                            }).toList()
                    ),
                    Stack(children: [
                        new Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                                new Padding(
                                    padding: EdgeInsets.all(32.0),
                                    child: new Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                            showBackBtn ? IconButton(onPressed: () // Skip button
                                            {
                                                if (_tabController.index != 0)
                                                {
                                                    _tabController.animateTo(_tabController.index - 1);
                                                }

                                                _changed(false, "finishBtn");

                                                if (_tabController.index != 0)
                                                    _changed(true, "backBtn");
                                                else
                                                    _changed(false, "backBtn");
                                            }, 
                                            icon: new Icon(Icons.navigate_before), iconSize: 52.0,) : new Container(),
                                            showFinishBtn ? FlatButton(child: new Text("FINISH", textScaleFactor: 1.1,), onPressed: ()
                                            {
                                                //We went through the introduction so let's tag this as disabled for next runs
                                                Utils.writeStrFile("{ \"disableFirstTime\": true }", "disableFirstTime.json", true);

                                                //Let's go to the login page
                                                Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => LoginPageMain()));
                                            },) : IconButton(onPressed: () 
                                            {
                                                if (_tabController.index == myTabs.length-2)
                                                {
                                                    _changed(true, "finishBtn");
                                                }

                                                if (_tabController.index != myTabs.length-1)
                                                    _tabController.animateTo(_tabController.index + 1);

                                                if (_tabController.index != 0)
                                                    _changed(true, "backBtn");
                                                else
                                                    _changed(false, "backBtn");
                                            },
                                            icon: new Icon(Icons.navigate_next), iconSize: 52.0,)
                                        ],
                                    ),
                                )
                            ],
                        ),                            
                ],),
            ],)
        );
    }
}