import 'package:flutter/material.dart';
import 'splashScreen.dart';
import 'dart:io' show Platform;

import 'package:flutter/foundation.dart'
    show debugDefaultTargetPlatformOverride;
import 'package:flutter/material.dart';

import 'package:color_panel/color_panel.dart';
import 'package:file_chooser/file_chooser.dart' as file_chooser;
import 'package:menubar/menubar.dart';

void main() 
{
    debugDefaultTargetPlatformOverride = TargetPlatform.fuchsia;
    runApp(Main());
}

class Main extends StatelessWidget 
{
    @override
    Widget build(BuildContext context) 
    {
        return MaterialApp(
            title: 'Fitness',
            theme: ThemeData(
                primarySwatch: Colors.blue,
            ),
            home: SplashScreenAnimation(),

        );
    }
}

