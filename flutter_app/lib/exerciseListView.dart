import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'preExercise.dart';

class ExerciseList extends StatefulWidget 
{
  final List userData;
  
  const ExerciseList({Key key, this.userData}) : super(key: key);

  @override
  _ExerciseListState createState() => new _ExerciseListState(userData);
}

class _ExerciseListState extends State<ExerciseList> with SingleTickerProviderStateMixin {
  List items;
  final List userData;
  
  _ExerciseListState(this.userData);


  Future<List> grabData() async {
    final response = await http.post("https://stardebris.net/exercises.php", body: {});
    var jsonData = json.decode(response.body);

    //Let's put the data in the fucking list
    items = jsonData;
    setState(() {});

    return jsonData;
  }

  @override
  void initState() {
    super.initState();
    grabData();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (items == null)
      return new Scaffold(
          body: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
        Padding(padding: EdgeInsets.all(32.0), child: CircularProgressIndicator())
      ]));
    else
      return new Scaffold(
          body: new Stack(children: [
        ListView.builder(
          itemCount: items.length,
          padding: EdgeInsets.all(8.0),
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          itemBuilder: (BuildContext context, int index) {
            return GestureDetector(
                onTap: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => PreExercise(items[index], userData)));
                },
                child: new Card(
                  elevation: 8.0,
                  margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
                  child: Container(
                    decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
                    child: ListTile(
                        contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                        leading: Container(
                          padding: EdgeInsets.only(right: 12.0),
                          decoration: new BoxDecoration(
                              border: new Border(
                                  right: new BorderSide(width: 1.0, color: Colors.white24))),
                          child: Icon(Icons.fitness_center, color: Colors.white),
                        ),
                        title: Text(
                          items[index]["name"],
                          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                        subtitle: Row(
                          children: <Widget>[
                            Icon(Icons.linear_scale, color: Colors.yellowAccent),
                            Text(
                                "Estimated time " +
                                    new Duration(seconds: int.parse(items[index]["estimatedTime"]))
                                        .inMinutes
                                        .toString() +
                                    " mins",
                                style: TextStyle(color: Colors.white))
                          ],
                        ),
                        trailing:
                            Icon(Icons.keyboard_arrow_right, color: Colors.white, size: 30.0)),
                  ),
                ));
          },
        )
      ]));
  }
}
