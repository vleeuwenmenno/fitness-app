import 'package:flutter/material.dart';
import 'countdown.dart';
import 'utils.dart';
import 'outroPage.dart';

class Workout extends StatefulWidget {
  final List exercise;
  final List userData;

  Workout(this.exercise, this.userData);

  @override
  _WorkoutState createState() => new _WorkoutState(exercise, userData);
}

class _WorkoutState extends State<Workout> with SingleTickerProviderStateMixin {
  final List exercise;
  final List userData;
  
  _WorkoutState(this.exercise, this.userData);

  TabController _tabController;

  bool showBackBtn = false;
  bool showFinishBtn = false;

  void _changed(bool visibility, String field) {
    setState(() {
      if (field == "backBtn")
        showBackBtn = visibility;
      else if (field == "finishBtn") showFinishBtn = visibility;
    });
  }

  final List<Tab> myTabs = <Tab>[];

  @override
  void initState() {
    super.initState();
    for (var i = 0; i < exercise.length; i++) {
      myTabs.add(new Tab(
          child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Image(
            image: AssetImage("assets/" + exercise[i]["workout"] + ".png"),
            fit: BoxFit.cover,
            gaplessPlayback: true,
            height: 128,
          ),
//                            new Icon(Icons.schedule, size: 196.0),
          Text(exercise[i]["workout"],
              style: TextStyle(fontStyle: FontStyle.normal, fontSize: 48.0)),
          Text(
              exercise[i]["amount"].toString() +
                  (exercise[i]["workout"] == "rest" ? " seconds" : "x"),
              style: TextStyle(fontStyle: FontStyle.italic, fontSize: 32.0)),

          (exercise[i]["workout"] == "rest"
              ? CountDown(
                  renderSemanticLabel: (count) {
                    if (count == exercise[i]["amount"]) {
                      return "Start";
                    }
                    return "$count";
                  },
                  beginCount: exercise[i]["amount"],
                  endCount: 0,
                  onPress: (ctr) {
                    if (ctr.isAnimating) {
                      return Future.value(false);
                    }
                    return Future.value(true);
                  })
              : Text(''))
        ],
      )));
    }

    setState(() {});

    _tabController = new TabController(vsync: this, length: exercise.length);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        // appBar: new AppBar(
        //     title: new Text("Introduction"),
        // ),
        body: new Stack(
      children: <Widget>[
        new TabBarView(
            physics: new NeverScrollableScrollPhysics(),
            controller: _tabController,
            children: myTabs.map((Tab tab) {
              return tab;
            }).toList()),
        Stack(
          children: [
            new Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                new Padding(
                  padding: EdgeInsets.all(32.0),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      showBackBtn
                          ? IconButton(
                              onPressed: () // Skip button
                                  {
                                if (_tabController.index != 0) {
                                  _tabController.animateTo(_tabController.index - 1);
                                }

                                _changed(false, "finishBtn");

                                if (_tabController.index != 0)
                                  _changed(true, "backBtn");
                                else
                                  _changed(false, "backBtn");
                              },
                              icon: new Icon(Icons.navigate_before),
                              iconSize: 52.0,
                            )
                          : new Container(),
                      showFinishBtn
                          ? FlatButton(
                              child: new Text(
                                "FINISH",
                                textScaleFactor: 1.1,
                              ),
                              onPressed: () {
                                //We went through the introduction so let's tag this as disabled for next runs
                                Utils.writeStrFile("{ \"disableFirstTime\": true }",
                                    "disableFirstTime.json", true);
                                Navigator.pushReplacement(context,
                                   MaterialPageRoute(builder: (context) => OutroPage(userData)));
                              },
                            )
                          : IconButton(
                              onPressed: () {
                                if (_tabController.index == myTabs.length - 2) {
                                  _changed(true, "finishBtn");
                                }

                                if (_tabController.index != myTabs.length - 1)
                                  _tabController.animateTo(_tabController.index + 1);

                                if (_tabController.index != 0)
                                  _changed(true, "backBtn");
                                else
                                  _changed(false, "backBtn");
                              },
                              icon: new Icon(Icons.navigate_next),
                              iconSize: 52.0,
                            )
                    ],
                  ),
                )
              ],
            ),
          ],
        ),
      ],
    ));
  }
}
