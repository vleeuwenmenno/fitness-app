import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'workout.dart';

class PreExercise extends StatefulWidget {
  final Map exercise;
  final List userData;
  
  PreExercise(this.exercise, this.userData);

  @override
  _PreExerciseState createState() => new _PreExerciseState(exercise, userData);
}

class _PreExerciseState extends State<PreExercise> with SingleTickerProviderStateMixin {
  final Map exercise;
  final List userData;
  
  _PreExerciseState(this.exercise, this.userData);

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List jsonData = json.decode(exercise["exerciseData"])["exercise"];

    return new Scaffold(
        appBar: new AppBar(title: Text("Exercise overview")),
        bottomNavigationBar: BottomNavigationBar(
            currentIndex: 1, // this will be set when a new tab is tapped
            items: [
              BottomNavigationBarItem(
                icon: new Icon(Icons.edit),
                title: new Text('Edit'),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.fitness_center),
                title: Text('Start'),
              )
            ],
            onTap: (value) {
              if (value == 1) {
                //Show actual exercise
                  Navigator.push(
                          context, MaterialPageRoute(builder: (context) => Workout(jsonData, userData)));
              } else if (value == 0) {
                //Open edit menu
                Fluttertoast.showToast(
                  msg: "This function is not yet implemented :(",
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIos: 2,
                );
                //TODO: Replace this code with actual editor code...
              }
            }),
        body: new Stack(children: [
          ListView.builder(
            itemCount: jsonData.length,
            padding: EdgeInsets.all(16.0),
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemBuilder: (BuildContext context, int index) {
              String capitalize(String s) => s[0].toUpperCase() + s.substring(1);

              return Card(
                  child: new Padding(
                      padding: EdgeInsets.all(8.0),
                      child: new ListTile(
                          leading: Image(
                            image: AssetImage("assets/" + jsonData[index]["workout"] + ".png"),
                            fit: BoxFit.cover,
                            gaplessPlayback: true,
                            height: 64,
                          ),
                          title: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(capitalize(jsonData[index]["workout"]),
                                  style: TextStyle(fontStyle: FontStyle.normal, fontSize: 24.0)),
                              Text(
                                  jsonData[index]["amount"].toString() +
                                      (jsonData[index]["workout"] == "rest" ? " seconds" : "x"),
                                  style: TextStyle(fontStyle: FontStyle.italic, fontSize: 16.0))
                            ],
                          ))));
            },
          )
        ]));
  }
}
